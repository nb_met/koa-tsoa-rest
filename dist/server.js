"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const helmet = require("koa-helmet");
const dist_1 = require("koa-winston/dist");
const routes_1 = require("./routes");
const Logger_1 = require("./Logger");
// todo
// cors
Logger_1.Logger.getInstance().info('Ready');
const app = new Koa();
const router = new Router();
app.use(bodyParser());
app.use(helmet());
app.use(dist_1.logger({ logger: Logger_1.Logger.getInstance() }));
app.use(router.routes());
routes_1.RegisterRoutes(router);
app.listen(3000);
Logger_1.Logger.getInstance().info('Server running on port 3000');
// 작업 고려 대상
// typescript typeorm
//# sourceMappingURL=server.js.map